package tests;

import helperClasses.DriverFactory;
import helperClasses.BaseTestClass;
import helperClasses.SeleniumUtil;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;


import static helperClasses.SeleniumUtil.*;
import static helperClasses.TestProperty.URL;

public class TraditionalTests extends BaseTestClass {
    @Test
    public void validateQueriesOfLife() throws IOException {
        openURL(URL);
        verifyPageTitle("Queries of Life");
    }

    @Test
    public void validateRedfinLoaded() throws IOException {
        openURL("https://www.zillow.com/homedetails/3109-Golden-Valley-Rd-Golden-Valley-MN-55422/1850402_zpid/");
    }
    

}
